FROM python:3.5-jessie
LABEL description="gitlab ci test."

USER root
RUN apt-get update && mkdir /root/.pip
COPY scripts/.pypirc /root/
COPY scripts/pip.conf /root/.pip/
RUN pip install flask pytest pytest-cov flake8 isort

WORKDIR /opt/gitlab_ci_test/
COPY . .

ENTRYPOINT ["sh", "entrypoint.sh"]
