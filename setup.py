# coding=utf-8
from setuptools import setup
packages = [
    'sample'
]
setup(
    name="gitlab_ci_test",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    author="fc",
    author_email="chaofan0110@163.com",
    description="test gitlab ci and auto deploy",
    long_description="Test gitlab CI CD auto Deploy",
    license="fc",
    keywords="gitlab_ci_test",
    packages=packages,
    package_dir={'sample': 'sample'},
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python 3',
        'Intended Audience :: Developers',
        'License :: fc Licence',
        'Operating System :: Mac OS',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    include_package_data=True,
    extras_require={},

    entry_points={
        'console_scripts': [
            'fanc_test = sample.controller:main'
        ]
    }
)
