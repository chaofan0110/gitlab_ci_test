.PHONY: test flake8 package clean

test:
	PYTHONPATH=. pipenv run py.test tests

flake8:
	pipenv run flake8 --ignore=E501,F401,E128,E402,E731,F821 sample

package:
	pipenv run python setup.py sdist

upload:
	pipenv run twine -r local dist/*
	rm -rf dist

clean:
	rm -fr build dist .egg *.egg-info .cache .pytest_cache
